# Aganitha UI Library

## Pre-requisite
1. Make sure `lerna` is installed and is up to date.
```
npm install -g lerna
```
2. At the project root, run `npm install`
3. Now, setup dependencies for packages by running `lerna bootstrap`.

## Component development
1. Run `lerna create <component-name> [--yes]` to create a folder for the component.
```
## examples

# creating a scoped component
lerna create @akm-hubs/table --yes
```

2. Adding dependencies for the component
```
# If you are creating a react component, add react as both dev and peer dependency
lerna add react --dev --scope=@akm-hubs/table
lerna add react --peer --scope=@akm-hubs/table

# adding dependencies to multiple packages
lerna add react --dev --scope '{@akm-hubs/button,@akm-hubs/table}'

# adding internal dependencies
lerna add @akm-hubs/table --scope=@akm-hubs/core
```

3. Adding builder script dependencies to the component
```
# add @akm-hubs/builder as a dev and peer dependency to your component
lerna add @akm-hubs/builder --dev --scope=@akm-hubs/table
lerna add @akm-hubs/builder --peer --scope=@akm-hubs/table
```

4. Update component's `package.json`
    1. Add a build script
    ```
    "scripts": {
        "build": "builder",
        ...
    },
    ```

    2. Update `main`, `module` and `src` keywords.
    ```
    "main": "dist/<table>.cjs.js",
    "module": "dist/<table>.esm.js",
    "src": "lib/<table>.js",
    ```

5. Add a `docs` folder to the package with a file in the following format `<component>.stories.js`. Check out `packages/table/docs/table.stories.js` for an example.

6. Run `npm run storybook`. Go to `http://192.168.1.4:6006/` in your browser.

7. Develop your component. Once completed, build your component by running `npm run build` at the root. This should create a `dist` folder within the package. Commit this to version control and push to remote.

8. Now run `lerna publish --registry <URL>`. Select appropriate versions when prompted. Once completed, you can visit [aganitha-npm-registry]() to check if it was pushed to the registry.

## Viewing the component library
1. Run `npm run storybook`

2. Go to `http://192.168.1.4:6006/` in your browser.

## Limitations

1. Add support for CSS Modules, SCSS in the builder script.

## Resources

- [lerna](https://github.com/lerna/lerna)

- [lerna example](https://dev.to/davixyz/writing-your-first-react-ui-library-part-1-lerna-17kc)