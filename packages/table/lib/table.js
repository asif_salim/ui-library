import React, { useState, useRef, useEffect, Fragment } from 'react';
// import { ReactTabulator } from "react-tabulator";
import "react-tabulator/lib/styles.css"; // default theme
import "react-tabulator/css/semantic-ui/tabulator_semantic-ui.min.css"; // use Theme(s)
const ReactTabulator = require('react-tabulator')['ReactTabulator']

export function merge2Dicts(a, b) {
  // not inplace
  // overrides with 2nd dict if a key is same in both dicts
  let out = {};

  for (let key in a) {
    out[key] = a[key]
  }

  for (let key in b) {
    out[key] = b[key]
  }

  return out;
}


export function AcogTabulator(props) {
  let table = null;
  // const theme = useTheme();
  // const md_match = useMediaQuery(theme.breakpoints.down("md"));
  const ref = useRef(null)
  // const {advancedToggle} = useContext(advancedModeContext)
  const { advancedToggle } = props.advancedModeContext ? props.advancedModeContext : false
  const multivalcol = props.multivalcol ? props.multivalcol : [];
  const htmlcol = props.htmlcol ? props.htmlcol : [];
  const iconcol = props.iconcol ? props.iconcol : [];
  const noSortCol = props.noSortCol ? props.noSortCol : [];
  // console.log(props.showcols)
  var columns = [];
  const [finalcolumns, SetFinalColumns] = useState(columns)
  useEffect(() => {
    SetFinalColumns(columns)
  }, [props.showcols])
  useEffect(() => {
    if (ref && ref.current && ref.current.table) {
      ref.current.table.redraw(true)
      //  console.log('redraw')
    }
  })
  //  console.log(finalcolumns)
  //  console.log(props.data)
  const updateColProps = (tmp, props, col) => {
    if (props.columnOptions && props.columnOptions[col]) {
      tmp["title"] = props.columnOptions[col]["title"] ? props.columnOptions[col]["title"] : tmp["title"]
      tmp["headerFilter"] = props.columnOptions[col]["search"] === false ? "" : tmp["headerFilter"]
    }
  }

  if (props.data === undefined || props.data === null || props.data.length === 0) {
    return null;
  }

  const column_names = Object.keys(props.data[0]).filter(
    (name) =>
      name.startsWith("__") !== true &&
      name !== "tableData" &&
      htmlcol.includes(name) !== true &&
      multivalcol.includes(name) !== true &&
      iconcol.includes(name) !== true
  );
  for (var col of column_names) {
    const tmp = {
      title: col.toUpperCase(),
      field: col,
      formatter: "textarea",
      visible: props.showcols !== undefined ? props.showcols[col] : true,
      headerFilter: "input",
      headerSort: noSortCol.length > 0 && noSortCol.includes(col) ? false : true
    }

    updateColProps(tmp, props, col)
    columns.push(tmp);
  }

  for (var hcol of htmlcol) {
    //  console.log(columns)
    if (Object.keys(props.data[0]).includes(hcol)) {
      const tmp = {
        title: hcol.toUpperCase(),
        field: hcol,
        formatter: function (cell, formatterParams) {
          var value = cell.getValue();
          var field = "<p style='white-space: normal; word-wrap: break-word;'>" + value + "</p>";
          return field;
        },
        visible: 'showcols' in props ? props.showcols[hcol] : true,
        headerFilter: "input",
        headerSort: noSortCol.length > 0 && noSortCol.includes(hcol) ? false : true
      }
      updateColProps(tmp, props, col)
      columns.push(tmp);
    }
  }

  for (var mcol of multivalcol) {
    if (Object.keys(props.data[0]).includes(mcol)) {
      const tmp = {
        title: mcol.toUpperCase(),
        field: mcol,
        formatter: function (cell, formatterParams) {
          var value = cell.getValue();
          var field = "<ul>";
          if (typeof value === "string") {
            // console.log(value)
            value = JSON.parse(value);
          }
          // console.log(value)
          for (var val of value) {
            field += "<li style='white-space: normal; word-wrap: break-word;'>" + val + "</li>";
          }
          field += "</ul>";
          return field;
        },
        visible: 'showcols' in props ? props.showcols[mcol] : true,
        headerFilter: "input",
        headerSort: noSortCol.length > 0 && noSortCol.includes(mcol) ? false : true
      }

      updateColProps(tmp, props, col)
      columns.push(tmp);
    }
  }

  for (var icol of iconcol) {
    if (Object.keys(props.data[0]).includes(icol)) {
      const tmp = {
        title: icol.toUpperCase(),
        field: icol,
        formatter: function (cell, formatterParams) {
          var value = cell.getValue();
          return "<span style='color:" + value['color'] + "; font-weight:bold;'>" + value['val'] + "</span>"
        },
        tooltip: function (cell) {
          var type = cell.getValue()["type"];
          return type;
        },
        sorter: function (a, b, aRow, bRow, column, dir, sorterParams) {
          // console.log(a)
          // console.log(b)
          return a['val'] - b['val']
        },
        visible: props.showcols[icol],
      }

      updateColProps(tmp, props, col)
      columns.push(tmp);
    }
  }


  let options = {
    movableColumns: true,
    downloadReady: (fileContents, blob) => {
      return blob
    }
  };

  const downloadExcel = () => {
    // let xlsx = require("xlsx")

    let fileName = props.title
    if (!('title' in props)) {
      fileName = "export"
    }

    ref.current.table.download('csv', `${fileName}.csv`);

  };

  if (finalcolumns.length === 0) {
    return null
  }

  if ('options' in props) {
    options = merge2Dicts(options, props.options)
  }

  if ('pagination' in props && props.pagination === false) {
    options['pagination'] = false
  } else {
    options['pagination'] = "local"
    options['paginationSize'] = 10
  }

  if (finalcolumns && finalcolumns.length !== 0) {
    columns = add_sorter_param_to_doses(finalcolumns)
  }

  return (
    <Fragment>
      {advancedToggle && [
        <Button
          variant="contained"
          onClick={downloadExcel}
        >
          Export
        </Button>
      ]}


      {/* Changed to ReactTabulator as pagination was not working */}
      <ReactTabulator
        ref={ref}
        layout={"fitColumns"}
        columns={finalcolumns}
        data={props.data}
        options={options}
        rowClick={props.rowClick}
        {...props.additional_props}
      />

    </Fragment>
  );
}

function add_sorter_param_to_doses(columns) {

  let new_cols = []
  for (var col of columns) {
    if (col && col.title && col.title === "DOSES") {
      col["sorter"] = "number";
    }
    new_cols.push(col)
  }

  return new_cols;
}