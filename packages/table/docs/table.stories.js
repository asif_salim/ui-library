import React from 'react';
// We want to always get from source
import { AcogTabulator } from '../lib/table'

export default { title: 'AcogTabulator' };

export const primary = () => <AcogTabulator 
  data={[
    {name: 'bob', marks: 25},
    {name: 'mcaw', marks: 25},
    {name: 'peter', marks: 25},
  ]}
/>;
