'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

var React = require('react');

function _interopDefaultLegacy (e) { return e && typeof e === 'object' && 'default' in e ? e : { 'default': e }; }

var React__default = /*#__PURE__*/_interopDefaultLegacy(React);

function _extends() {
  _extends = Object.assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  return _extends.apply(this, arguments);
}

function _slicedToArray(arr, i) {
  return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest();
}

function _arrayWithHoles(arr) {
  if (Array.isArray(arr)) return arr;
}

function _iterableToArrayLimit(arr, i) {
  if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return;
  var _arr = [];
  var _n = true;
  var _d = false;
  var _e = undefined;

  try {
    for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) {
      _arr.push(_s.value);

      if (i && _arr.length === i) break;
    }
  } catch (err) {
    _d = true;
    _e = err;
  } finally {
    try {
      if (!_n && _i["return"] != null) _i["return"]();
    } finally {
      if (_d) throw _e;
    }
  }

  return _arr;
}

function _unsupportedIterableToArray(o, minLen) {
  if (!o) return;
  if (typeof o === "string") return _arrayLikeToArray(o, minLen);
  var n = Object.prototype.toString.call(o).slice(8, -1);
  if (n === "Object" && o.constructor) n = o.constructor.name;
  if (n === "Map" || n === "Set") return Array.from(o);
  if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen);
}

function _arrayLikeToArray(arr, len) {
  if (len == null || len > arr.length) len = arr.length;

  for (var i = 0, arr2 = new Array(len); i < len; i++) {
    arr2[i] = arr[i];
  }

  return arr2;
}

function _nonIterableRest() {
  throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");
}

function _createForOfIteratorHelper(o, allowArrayLike) {
  var it;

  if (typeof Symbol === "undefined" || o[Symbol.iterator] == null) {
    if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === "number") {
      if (it) o = it;
      var i = 0;

      var F = function F() {};

      return {
        s: F,
        n: function n() {
          if (i >= o.length) return {
            done: true
          };
          return {
            done: false,
            value: o[i++]
          };
        },
        e: function e(_e2) {
          throw _e2;
        },
        f: F
      };
    }

    throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");
  }

  var normalCompletion = true,
      didErr = false,
      err;
  return {
    s: function s() {
      it = o[Symbol.iterator]();
    },
    n: function n() {
      var step = it.next();
      normalCompletion = step.done;
      return step;
    },
    e: function e(_e3) {
      didErr = true;
      err = _e3;
    },
    f: function f() {
      try {
        if (!normalCompletion && it["return"] != null) it["return"]();
      } finally {
        if (didErr) throw err;
      }
    }
  };
}

function styleInject(css, ref) {
  if (ref === void 0) ref = {};
  var insertAt = ref.insertAt;

  if (!css || typeof document === 'undefined') {
    return;
  }

  var head = document.head || document.getElementsByTagName('head')[0];
  var style = document.createElement('style');
  style.type = 'text/css';

  if (insertAt === 'top') {
    if (head.firstChild) {
      head.insertBefore(style, head.firstChild);
    } else {
      head.appendChild(style);
    }
  } else {
    head.appendChild(style);
  }

  if (style.styleSheet) {
    style.styleSheet.cssText = css;
  } else {
    style.appendChild(document.createTextNode(css));
  }
}

var css_248z = ".multi-value-formatter-content span {\n  padding: 2px 4px;\n  border-radius: 2px;\n  border: 1px solid lightgrey;\n  margin-right: 4px;\n}\n\n/* --------- customized react-tags styles */\n.react-tags {\n  padding: 0px 0 0 6px !important;\n  border: none !important;\n}\n.react-tags__selected-tag:after {\n  /* TODO: for now, don't show the delete icon \"X\" for react-tags */\n  display: none !important;\n}\n.react-tags__selected-tag {\n  padding: 2px 4px !important;\n}\n\n/* --------- default react-tags styles */\n.react-tags__search input {\n  width: 100% !important;\n}\n\n.react-tags {\n  position: relative;\n  padding: 6px 0 0 6px;\n  border: 1px solid #D1D1D1;\n  border-radius: 1px;\n\n  /* shared font styles */\n  font-size: 1em;\n  line-height: 1.2;\n\n  /* clicking anywhere will focus the input */\n  cursor: text;\n}\n\n.react-tags.is-focused {\n  border-color: #B1B1B1;\n}\n\n.react-tags__selected {\n  display: inline;\n}\n\n.react-tags__selected-tag {\n  display: inline-block;\n  box-sizing: border-box;\n  margin: 0 6px 6px 0;\n  padding: 6px 8px;\n  border: 1px solid #D1D1D1;\n  border-radius: 2px;\n  background: #F1F1F1;\n\n  /* match the font styles */\n  font-size: inherit;\n  line-height: inherit;\n}\n\n.react-tags__selected-tag:after {\n  content: '\\2715';\n  color: #AAA;\n  margin-left: 8px;\n}\n\n.react-tags__selected-tag:hover,\n.react-tags__selected-tag:focus {\n  border-color: #B1B1B1;\n}\n\n.react-tags__search {\n  display: inline-block;\n\n  /* match tag layout */\n  padding: 7px 2px;\n  margin-bottom: 6px;\n\n  /* prevent autoresize overflowing the container */\n  max-width: 100%;\n}\n\n@media screen and (min-width: 30em) {\n\n  .react-tags__search {\n    /* this will become the offsetParent for suggestions */\n    position: relative;\n  }\n\n}\n\n.react-tags__search input {\n  /* prevent autoresize overflowing the container */\n  max-width: 100%;\n\n  /* remove styles and layout from this element */\n  margin: 0;\n  padding: 0;\n  border: 0;\n  outline: none;\n\n  /* match the font styles */\n  font-size: inherit;\n  line-height: inherit;\n}\n\n.react-tags__search input::-ms-clear {\n  display: none;\n}\n\n.react-tags__suggestions {\n  position: absolute;\n  top: 100%;\n  left: 0;\n  width: 100%;\n}\n\n@media screen and (min-width: 30em) {\n\n  .react-tags__suggestions {\n    width: 240px;\n  }\n\n}\n\n.react-tags__suggestions ul {\n  margin: 4px -1px;\n  padding: 0;\n  list-style: none;\n  background: white;\n  border: 1px solid #D1D1D1;\n  border-radius: 2px;\n  box-shadow: 0 2px 6px rgba(0, 0, 0, 0.2);\n}\n\n.react-tags__suggestions li {\n  border-bottom: 1px solid #ddd;\n  padding: 6px 8px;\n}\n\n.react-tags__suggestions li mark {\n  text-decoration: underline;\n  background: none;\n  font-weight: 600;\n}\n\n.react-tags__suggestions li:hover {\n  cursor: pointer;\n  background: #eee;\n}\n\n.react-tags__suggestions li.is-active {\n  background: #b7cfe0;\n}\n\n.react-tags__suggestions li.is-disabled {\n  opacity: 0.5;\n  cursor: auto;\n}";
styleInject(css_248z);
var css_248z$1 = "/* Tabulator v4.9.3 (c) Oliver Folkerd */\n.tabulator{position:relative;background-color:#fff;overflow:hidden;font-size:14px;text-align:left;width:100%;margin:1em 0;border:1px solid rgba(34,36,38,.15);box-shadow:none;border-radius:.28571rem;color:rgba(0,0,0,.87);transform:translatez(0)}.tabulator[tabulator-layout=fitDataFill] .tabulator-tableHolder .tabulator-table{min-width:100%}.tabulator[tabulator-layout=fitDataTable]{display:inline-block}.tabulator.tabulator-block-select{-webkit-user-select:none;-ms-user-select:none;user-select:none}.tabulator .tabulator-header{position:relative;box-sizing:border-box;width:100%;border-bottom:1px solid rgba(34,36,38,.1);background-color:#f9fafb;box-shadow:none;color:rgba(0,0,0,.87);font-style:none;font-weight:700;text-transform:none;white-space:nowrap;overflow:hidden;-moz-user-select:none;-khtml-user-select:none;-webkit-user-select:none;-o-user-select:none}.tabulator .tabulator-header.tabulator-header-hidden{display:none}.tabulator .tabulator-header .tabulator-col{display:inline-block;position:relative;box-sizing:border-box;background-color:#f9fafb;text-align:left;vertical-align:bottom;overflow:hidden}.tabulator .tabulator-header .tabulator-col.tabulator-moving{position:absolute;border:1px solid #999;background:#dae1e7;pointer-events:none}.tabulator .tabulator-header .tabulator-col .tabulator-col-content{box-sizing:border-box;position:relative;padding:.92857em .78571em}.tabulator .tabulator-header .tabulator-col .tabulator-col-content .tabulator-header-menu-button{padding:0 8px}.tabulator .tabulator-header .tabulator-col .tabulator-col-content .tabulator-header-menu-button:hover{cursor:pointer;opacity:.6}.tabulator .tabulator-header .tabulator-col .tabulator-col-content .tabulator-col-title-holder{position:relative}.tabulator .tabulator-header .tabulator-col .tabulator-col-content .tabulator-col-title{box-sizing:border-box;width:100%;white-space:nowrap;overflow:hidden;text-overflow:ellipsis;vertical-align:bottom}.tabulator .tabulator-header .tabulator-col .tabulator-col-content .tabulator-col-title .tabulator-title-editor{box-sizing:border-box;width:100%;border:1px solid #999;padding:1px;background:#fff}.tabulator .tabulator-header .tabulator-col .tabulator-col-content .tabulator-col-title .tabulator-header-menu-button+.tabulator-title-editor{width:calc(100% - 22px)}.tabulator .tabulator-header .tabulator-col .tabulator-col-content .tabulator-col-sorter{display:-ms-flexbox;display:flex;-ms-flex-align:center;align-items:center;position:absolute;top:0;bottom:0;right:4px}.tabulator .tabulator-header .tabulator-col .tabulator-col-content .tabulator-col-sorter .tabulator-arrow{width:0;height:0;border-left:6px solid transparent;border-right:6px solid transparent;border-bottom:6px solid #bbb}.tabulator .tabulator-header .tabulator-col.tabulator-col-group .tabulator-col-group-cols{position:relative;display:-ms-flexbox;display:flex;border-top:1px solid #ddd;overflow:hidden;margin-right:-1px}.tabulator .tabulator-header .tabulator-col:first-child .tabulator-col-resize-handle.prev{display:none}.tabulator .tabulator-header .tabulator-col .tabulator-header-filter{position:relative;box-sizing:border-box;margin-top:2px;width:100%;text-align:center}.tabulator .tabulator-header .tabulator-col .tabulator-header-filter textarea{height:auto!important}.tabulator .tabulator-header .tabulator-col .tabulator-header-filter svg{margin-top:3px}.tabulator .tabulator-header .tabulator-col .tabulator-header-filter input::-ms-clear{width:0;height:0}.tabulator .tabulator-header .tabulator-col.tabulator-sortable .tabulator-col-title{padding-right:25px}.tabulator .tabulator-header .tabulator-col.tabulator-sortable:hover{cursor:pointer;background-color:#dae1e7}.tabulator .tabulator-header .tabulator-col.tabulator-sortable[aria-sort=none] .tabulator-col-content .tabulator-col-sorter{color:#bbb}.tabulator .tabulator-header .tabulator-col.tabulator-sortable[aria-sort=none] .tabulator-col-content .tabulator-col-sorter .tabulator-arrow{border-top:none;border-bottom:6px solid #bbb}.tabulator .tabulator-header .tabulator-col.tabulator-sortable[aria-sort=asc] .tabulator-col-content .tabulator-col-sorter{color:#666}.tabulator .tabulator-header .tabulator-col.tabulator-sortable[aria-sort=asc] .tabulator-col-content .tabulator-col-sorter .tabulator-arrow{border-top:none;border-bottom:6px solid #666}.tabulator .tabulator-header .tabulator-col.tabulator-sortable[aria-sort=desc] .tabulator-col-content .tabulator-col-sorter{color:#666}.tabulator .tabulator-header .tabulator-col.tabulator-sortable[aria-sort=desc] .tabulator-col-content .tabulator-col-sorter .tabulator-arrow{border-bottom:none;border-top:6px solid #666;color:#666}.tabulator .tabulator-header .tabulator-col.tabulator-col-vertical .tabulator-col-content .tabulator-col-title{-ms-writing-mode:tb-rl;writing-mode:vertical-rl;text-orientation:mixed;display:-ms-flexbox;display:flex;-ms-flex-align:center;align-items:center;-ms-flex-pack:center;justify-content:center}.tabulator .tabulator-header .tabulator-col.tabulator-col-vertical.tabulator-col-vertical-flip .tabulator-col-title{transform:rotate(180deg)}.tabulator .tabulator-header .tabulator-col.tabulator-col-vertical.tabulator-sortable .tabulator-col-title{padding-right:0;padding-top:20px}.tabulator .tabulator-header .tabulator-col.tabulator-col-vertical.tabulator-sortable.tabulator-col-vertical-flip .tabulator-col-title{padding-right:0;padding-bottom:20px}.tabulator .tabulator-header .tabulator-col.tabulator-col-vertical.tabulator-sortable .tabulator-col-sorter{-ms-flex-pack:center;justify-content:center;left:0;right:0;top:4px;bottom:auto}.tabulator .tabulator-header .tabulator-frozen{display:inline-block;position:absolute;z-index:10}.tabulator .tabulator-header .tabulator-frozen.tabulator-frozen-left{border-right:2px solid #ddd}.tabulator .tabulator-header .tabulator-frozen.tabulator-frozen-right{border-left:2px solid #ddd}.tabulator .tabulator-header .tabulator-calcs-holder{box-sizing:border-box;min-width:600%;background:#fff!important;border-top:1px solid #ddd;border-bottom:1px solid #ddd;overflow:hidden}.tabulator .tabulator-header .tabulator-calcs-holder .tabulator-row{background:#fff!important}.tabulator .tabulator-header .tabulator-calcs-holder .tabulator-row .tabulator-col-resize-handle{display:none}.tabulator .tabulator-header .tabulator-frozen-rows-holder{min-width:600%}.tabulator .tabulator-header .tabulator-frozen-rows-holder:empty{display:none}.tabulator .tabulator-tableHolder{position:relative;width:100%;white-space:nowrap;overflow:auto;-webkit-overflow-scrolling:touch}.tabulator .tabulator-tableHolder:focus{outline:none}.tabulator .tabulator-tableHolder .tabulator-placeholder{box-sizing:border-box;display:-ms-flexbox;display:flex;-ms-flex-align:center;align-items:center;width:100%}.tabulator .tabulator-tableHolder .tabulator-placeholder[tabulator-render-mode=virtual]{min-height:100%;min-width:100%}.tabulator .tabulator-tableHolder .tabulator-placeholder span{display:inline-block;margin:0 auto;padding:10px;color:#000;font-weight:700;font-size:20px}.tabulator .tabulator-tableHolder .tabulator-table{position:relative;display:inline-block;white-space:nowrap;overflow:visible;color:#333}.tabulator .tabulator-tableHolder .tabulator-table .tabulator-row.tabulator-calcs{font-weight:700;background:#f2f2f2!important}.tabulator .tabulator-tableHolder .tabulator-table .tabulator-row.tabulator-calcs.tabulator-calcs-top{border-bottom:2px solid #ddd}.tabulator .tabulator-tableHolder .tabulator-table .tabulator-row.tabulator-calcs.tabulator-calcs-bottom{border-top:2px solid #ddd}.tabulator .tabulator-col-resize-handle{position:absolute;right:0;top:0;bottom:0;width:5px}.tabulator .tabulator-col-resize-handle.prev{left:0;right:auto}.tabulator .tabulator-col-resize-handle:hover{cursor:ew-resize}.tabulator .tabulator-footer{padding:.78571em;border-top:1px solid rgba(34,36,38,.15);box-shadow:none;background:#f9fafb;text-align:right;color:rgba(0,0,0,.87);font-style:normal;font-weight:400;text-transform:none;white-space:nowrap;-ms-user-select:none;user-select:none;-moz-user-select:none;-khtml-user-select:none;-webkit-user-select:none;-o-user-select:none}.tabulator .tabulator-footer .tabulator-calcs-holder{box-sizing:border-box;width:calc(100% + 20px);margin:-.78571em -.78571em .78571em;text-align:left;background:#fff!important;border-bottom:1px solid #ddd;border-top:1px solid #ddd;overflow:hidden}.tabulator .tabulator-footer .tabulator-calcs-holder .tabulator-row{font-weight:700;background:#fff!important}.tabulator .tabulator-footer .tabulator-calcs-holder .tabulator-row .tabulator-col-resize-handle{display:none}.tabulator .tabulator-footer .tabulator-calcs-holder:only-child{margin-bottom:-.78571em;border-bottom:none}.tabulator .tabulator-footer .tabulator-paginator{color:#555;font-family:inherit;font-weight:inherit;font-size:inherit}.tabulator .tabulator-footer .tabulator-page-size{display:inline-block;margin:0 5px;padding:2px 5px;border:1px solid #aaa;border-radius:3px}.tabulator .tabulator-footer .tabulator-pages{margin:0 7px}.tabulator .tabulator-footer .tabulator-page{display:inline-block;margin:0 2px;padding:2px 5px;border:1px solid #aaa;border-radius:3px;background:hsla(0,0%,100%,.2)}.tabulator .tabulator-footer .tabulator-page.active{color:#d00}.tabulator .tabulator-footer .tabulator-page:disabled{opacity:.5}.tabulator .tabulator-footer .tabulator-page:not(.disabled):hover{cursor:pointer;background:rgba(0,0,0,.2);color:#fff}.tabulator .tabulator-loader{position:absolute;display:-ms-flexbox;display:flex;-ms-flex-align:center;align-items:center;top:0;left:0;z-index:100;height:100%;width:100%;background:rgba(0,0,0,.4);text-align:center}.tabulator .tabulator-loader .tabulator-loader-msg{display:inline-block;margin:0 auto;padding:10px 20px;border-radius:10px;background:#fff;font-weight:700;font-size:16px}.tabulator .tabulator-loader .tabulator-loader-msg.tabulator-loading{border:4px solid #333;color:#000}.tabulator .tabulator-loader .tabulator-loader-msg.tabulator-error{border:4px solid #d00;color:#590000}.tabulator .tabulator-tableHolder .tabulator-table .tabulator-row.positive,.tabulator .tabulator-tableHolder .tabulator-table .tabulator-row .tabulator-cell.positive{box-shadow:inset 0 0 0 #a3c293;background:#fcfff5!important;color:#21ba45!important}.tabulator .tabulator-tableHolder .tabulator-table .tabulator-row.positive:hover,.tabulator .tabulator-tableHolder .tabulator-table .tabulator-row .tabulator-cell.positive:hover{background:#f7ffe6!important;color:#13ae38!important}.tabulator .tabulator-tableHolder .tabulator-table .tabulator-row.negative,.tabulator .tabulator-tableHolder .tabulator-table .tabulator-row .tabulator-cell.negative{box-shadow:inset 0 0 0 #e0b4b4;background:#fff6f6!important;color:#db2828!important}.tabulator .tabulator-tableHolder .tabulator-table .tabulator-row.negative:hover,.tabulator .tabulator-tableHolder .tabulator-table .tabulator-row .tabulator-cell.negative:hover{background:#ffe7e7!important;color:#d41616!important}.tabulator .tabulator-tableHolder .tabulator-table .tabulator-row.error,.tabulator .tabulator-tableHolder .tabulator-table .tabulator-row .tabulator-cell.error{box-shadow:inset 0 0 0 #e0b4b4;background:#fff6f6!important;color:#db2828!important}.tabulator .tabulator-tableHolder .tabulator-table .tabulator-row.error:hover,.tabulator .tabulator-tableHolder .tabulator-table .tabulator-row .tabulator-cell.error:hover{background:#ffe7e7!important;color:#d12323!important}.tabulator .tabulator-tableHolder .tabulator-table .tabulator-row .tabulator-cell.warning,.tabulator .tabulator-tableHolder .tabulator-table .tabulator-row.warning{box-shadow:inset 0 0 0 #c9ba9b;background:#fffaf3!important;color:#f2c037!important}.tabulator .tabulator-tableHolder .tabulator-table .tabulator-row .tabulator-cell.warning:hover,.tabulator .tabulator-tableHolder .tabulator-table .tabulator-row.warning:hover{background:#fff4e4!important;color:#f1bb29!important}.tabulator .tabulator-tableHolder .tabulator-table .tabulator-row.active,.tabulator .tabulator-tableHolder .tabulator-table .tabulator-row .tabulator-cell.active{box-shadow:inset 0 0 0 rgba(0,0,0,.87);background:#e0e0e0!important;color:rgba(0,0,0,.87)!important}.tabulator .tabulator-tableHolder .tabulator-table .tabulator-row.active:hover,.tabulator .tabulator-tableHolder .tabulator-table .tabulator-row .tabulator-cell.active:hover{background:#f7ffe6!important;color:#13ae38!important}.tabulator .tabulator-tableHolder .tabulator-table .tabulator-row.active,.tabulator .tabulator-tableHolder .tabulator-table .tabulator-row.disabled:hover,.tabulator .tabulator-tableHolder .tabulator-table .tabulator-row .tabulator-cell.active{pointer-events:none;color:rgba(0,0,0,.2)}.tabulator.inverted{background:#333;color:hsla(0,0%,100%,.9);border:none}.tabulator.inverted .tabulator-header{background-color:rgba(0,0,0,.15);color:hsla(0,0%,100%,.9)}.tabulator.inverted .tabulator-header,.tabulator.inverted .tabulator-header .tabulator-col{border-color:hsla(0,0%,100%,.1)!important}.tabulator.inverted .tabulator-tableHolder .tabulator-table .tabulator-row{color:hsla(0,0%,100%,.9);border:none}.tabulator.inverted .tabulator-tableHolder .tabulator-table .tabulator-row .tabulator-cell{border-color:hsla(0,0%,100%,.1)!important}.tabulator.inverted .tabulator-footer{background:#fff}.tabulator.striped .tabulator-tableHolder .tabulator-table .tabulator-row:nth-child(2n){background-color:rgba(0,0,0,.05)!important}.tabulator.celled{border:1px solid rgba(34,36,38,.15)}.tabulator.celled .tabulator-header .tabulator-col,.tabulator.celled .tabulator-tableHolder .tabulator-table .tabulator-row .tabulator-cell{border-right:1px solid rgba(34,36,38,.1)}.tabulator[class*=\"single line\"] .tabulator-tableHolder .tabulator-table .tabulator-row .tabulator-cell{border-right:none}.tabulator.red{border-top:.2em solid #db2828}.tabulator.inverted.red{background-color:#db2828!important;color:#fff!important}.tabulator.orange{border-top:.2em solid #f2711c}.tabulator.inverted.orange{background-color:#f2711c!important;color:#fff!important}.tabulator.yellow{border-top:.2em solid #fbbd08}.tabulator.inverted.yellow{background-color:#fbbd08!important;color:#fff!important}.tabulator.olive{border-top:.2em solid #b5cc18}.tabulator.inverted.olive{background-color:#b5cc18!important;color:#fff!important}.tabulator.green{border-top:.2em solid #21ba45}.tabulator.inverted.green{background-color:#21ba45!important;color:#fff!important}.tabulator.teal{border-top:.2em solid #00b5ad}.tabulator.inverted.teal{background-color:#00b5ad!important;color:#fff!important}.tabulator.blue{border-top:.2em solid #2185d0}.tabulator.inverted.blue{background-color:#2185d0!important;color:#fff!important}.tabulator.violet{border-top:.2em solid #6435c9}.tabulator.inverted.violet{background-color:#6435c9!important;color:#fff!important}.tabulator.purple{border-top:.2em solid #a333c8}.tabulator.inverted.purple{background-color:#a333c8!important;color:#fff!important}.tabulator.pink{border-top:.2em solid #e03997}.tabulator.inverted.pink{background-color:#e03997!important;color:#fff!important}.tabulator.brown{border-top:.2em solid #a5673f}.tabulator.inverted.brown{background-color:#a5673f!important;color:#fff!important}.tabulator.grey{border-top:.2em solid #767676}.tabulator.inverted.grey{background-color:#767676!important;color:#fff!important}.tabulator.black{border-top:.2em solid #1b1c1d}.tabulator.inverted.black{background-color:#1b1c1d!important;color:#fff!important}.tabulator.padded .tabulator-header .tabulator-col .tabulator-col-content{padding:1em}.tabulator.padded .tabulator-header .tabulator-col .tabulator-col-content .tabulator-arrow{top:20px}.tabulator.padded .tabulator-tableHolder .tabulator-table .tabulator-row .tabulator-cell{padding:1em}.tabulator.padded.very .tabulator-header .tabulator-col .tabulator-col-content{padding:1.5em}.tabulator.padded.very .tabulator-header .tabulator-col .tabulator-col-content .tabulator-arrow{top:26px}.tabulator.padded.very .tabulator-tableHolder .tabulator-table .tabulator-row .tabulator-cell{padding:1.5em}.tabulator.compact .tabulator-header .tabulator-col .tabulator-col-content{padding:.5em .7em}.tabulator.compact .tabulator-header .tabulator-col .tabulator-col-content .tabulator-arrow{top:12px}.tabulator.compact .tabulator-tableHolder .tabulator-table .tabulator-row .tabulator-cell{padding:.5em .7em}.tabulator.compact.very .tabulator-header .tabulator-col .tabulator-col-content{padding:.4em .6em}.tabulator.compact.very .tabulator-header .tabulator-col .tabulator-col-content .tabulator-arrow{top:10px}.tabulator.compact.very .tabulator-tableHolder .tabulator-table .tabulator-row .tabulator-cell{padding:.4em .6em}.tabulator-row{position:relative;box-sizing:border-box;min-height:22px;border-bottom:1px solid rgba(34,36,38,.1)}.tabulator-row.tabulator-selectable:hover{box-shadow:inset 0 0 0 rgba(0,0,0,.87);background:#e0e0e0!important;color:rgba(0,0,0,.87)!important;cursor:pointer}.tabulator-row.tabulator-selected{background-color:#9abcea}.tabulator-row.tabulator-selected:hover{background-color:#769bcc;cursor:pointer}.tabulator-row.tabulator-moving{position:absolute;border-top:1px solid #ddd;border-bottom:1px solid #ddd;pointer-events:none!important;z-index:15}.tabulator-row .tabulator-row-resize-handle{position:absolute;right:0;bottom:0;left:0;height:5px}.tabulator-row .tabulator-row-resize-handle.prev{top:0;bottom:auto}.tabulator-row .tabulator-row-resize-handle:hover{cursor:ns-resize}.tabulator-row .tabulator-frozen{display:inline-block;position:absolute;background-color:inherit;z-index:10}.tabulator-row .tabulator-frozen.tabulator-frozen-left{border-right:2px solid #ddd}.tabulator-row .tabulator-frozen.tabulator-frozen-right{border-left:2px solid #ddd}.tabulator-row .tabulator-responsive-collapse{box-sizing:border-box;padding:5px;border-top:1px solid #ddd;border-bottom:1px solid #ddd}.tabulator-row .tabulator-responsive-collapse:empty{display:none}.tabulator-row .tabulator-responsive-collapse table{font-size:14px}.tabulator-row .tabulator-responsive-collapse table tr td{position:relative}.tabulator-row .tabulator-responsive-collapse table tr td:first-of-type{padding-right:10px}.tabulator-row .tabulator-cell{display:inline-block;position:relative;box-sizing:border-box;padding:.78571em;vertical-align:middle;white-space:nowrap;overflow:hidden;text-overflow:ellipsis}.tabulator-row .tabulator-cell:last-of-type{border-right:none}.tabulator-row .tabulator-cell.tabulator-editing{border:1px solid #1d68cd;outline:none;padding:0}.tabulator-row .tabulator-cell.tabulator-editing input,.tabulator-row .tabulator-cell.tabulator-editing select{border:1px;background:transparent}.tabulator-row .tabulator-cell.tabulator-validation-fail{border:1px solid #db2828}.tabulator-row .tabulator-cell.tabulator-validation-fail input,.tabulator-row .tabulator-cell.tabulator-validation-fail select{border:1px;background:transparent;color:#db2828}.tabulator-row .tabulator-cell:first-child .tabulator-col-resize-handle.prev{display:none}.tabulator-row .tabulator-cell.tabulator-row-handle{display:-ms-inline-flexbox;display:inline-flex;-ms-flex-align:center;align-items:center;-moz-user-select:none;-khtml-user-select:none;-webkit-user-select:none;-o-user-select:none}.tabulator-row .tabulator-cell.tabulator-row-handle .tabulator-row-handle-box{width:80%}.tabulator-row .tabulator-cell.tabulator-row-handle .tabulator-row-handle-box .tabulator-row-handle-bar{width:100%;height:3px;margin-top:2px;background:#666}.tabulator-row .tabulator-cell .tabulator-data-tree-branch{display:inline-block;vertical-align:middle;height:9px;width:7px;margin-top:-9px;margin-right:5px;border-bottom-left-radius:1px;border-left:2px solid #ddd;border-bottom:2px solid #ddd}.tabulator-row .tabulator-cell .tabulator-data-tree-control{display:-ms-inline-flexbox;display:inline-flex;-ms-flex-pack:center;justify-content:center;-ms-flex-align:center;align-items:center;vertical-align:middle;height:11px;width:11px;margin-right:5px;border:1px solid #333;border-radius:2px;background:rgba(0,0,0,.1);overflow:hidden}.tabulator-row .tabulator-cell .tabulator-data-tree-control:hover{cursor:pointer;background:rgba(0,0,0,.2)}.tabulator-row .tabulator-cell .tabulator-data-tree-control .tabulator-data-tree-control-collapse{display:inline-block;position:relative;height:7px;width:1px;background:transparent}.tabulator-row .tabulator-cell .tabulator-data-tree-control .tabulator-data-tree-control-collapse:after{position:absolute;content:\"\";left:-3px;top:3px;height:1px;width:7px;background:#333}.tabulator-row .tabulator-cell .tabulator-data-tree-control .tabulator-data-tree-control-expand{display:inline-block;position:relative;height:7px;width:1px;background:#333}.tabulator-row .tabulator-cell .tabulator-data-tree-control .tabulator-data-tree-control-expand:after{position:absolute;content:\"\";left:-3px;top:3px;height:1px;width:7px;background:#333}.tabulator-row .tabulator-cell .tabulator-responsive-collapse-toggle{display:-ms-inline-flexbox;display:inline-flex;-ms-flex-align:center;align-items:center;-ms-flex-pack:center;justify-content:center;-moz-user-select:none;-khtml-user-select:none;-webkit-user-select:none;-o-user-select:none;height:15px;width:15px;border-radius:20px;background:#666;color:#fff;font-weight:700;font-size:1.1em}.tabulator-row .tabulator-cell .tabulator-responsive-collapse-toggle:hover{opacity:.7}.tabulator-row .tabulator-cell .tabulator-responsive-collapse-toggle.open .tabulator-responsive-collapse-toggle-close{display:initial}.tabulator-row .tabulator-cell .tabulator-responsive-collapse-toggle.open .tabulator-responsive-collapse-toggle-open,.tabulator-row .tabulator-cell .tabulator-responsive-collapse-toggle .tabulator-responsive-collapse-toggle-close{display:none}.tabulator-row .tabulator-cell .tabulator-traffic-light{display:inline-block;height:14px;width:14px;border-radius:14px}.tabulator-row.tabulator-group{box-sizing:border-box;border-bottom:1px solid #999;border-right:1px solid #ddd;border-top:1px solid #999;padding:5px;padding-left:10px;background:#fafafa;font-weight:700;min-width:100%}.tabulator-row.tabulator-group:hover{cursor:pointer;background-color:rgba(0,0,0,.1)}.tabulator-row.tabulator-group.tabulator-group-visible .tabulator-arrow{margin-right:10px;border-left:6px solid transparent;border-right:6px solid transparent;border-top:6px solid #666;border-bottom:0}.tabulator-row.tabulator-group.tabulator-group-level-1{padding-left:30px}.tabulator-row.tabulator-group.tabulator-group-level-2{padding-left:50px}.tabulator-row.tabulator-group.tabulator-group-level-3{padding-left:70px}.tabulator-row.tabulator-group.tabulator-group-level-4{padding-left:90px}.tabulator-row.tabulator-group.tabulator-group-level-5{padding-left:110px}.tabulator-row.tabulator-group .tabulator-arrow{display:inline-block;width:0;height:0;margin-right:16px;border-top:6px solid transparent;border-bottom:6px solid transparent;border-right:0;border-left:6px solid #666;vertical-align:middle}.tabulator-row.tabulator-group span{margin-left:10px;color:#666}.tabulator-menu{position:absolute;display:inline-block;box-sizing:border-box;background:#fff;border:1px solid #ddd;box-shadow:0 0 5px 0 rgba(0,0,0,.2);font-size:14px;overflow-y:auto;-webkit-overflow-scrolling:touch;z-index:10000}.tabulator-menu .tabulator-menu-item{position:relative;box-sizing:border-box;padding:5px 10px;-webkit-user-select:none;-ms-user-select:none;user-select:none}.tabulator-menu .tabulator-menu-item.tabulator-menu-item-disabled{opacity:.5}.tabulator-menu .tabulator-menu-item:not(.tabulator-menu-item-disabled):hover{cursor:pointer;background:#f9fafb}.tabulator-menu .tabulator-menu-item.tabulator-menu-item-submenu{padding-right:25px}.tabulator-menu .tabulator-menu-item.tabulator-menu-item-submenu:after{display:inline-block;position:absolute;top:calc(5px + .4em);right:10px;height:7px;width:7px;content:\"\";border-width:1px 1px 0 0;border-style:solid;border-color:#ddd;vertical-align:top;transform:rotate(45deg)}.tabulator-menu .tabulator-menu-separator{border-top:1px solid #ddd}.tabulator-edit-select-list{position:absolute;display:inline-block;box-sizing:border-box;max-height:200px;background:#fff;border:1px solid #ddd;font-size:14px;overflow-y:auto;-webkit-overflow-scrolling:touch;z-index:10000}.tabulator-edit-select-list .tabulator-edit-select-list-item{padding:4px;color:#333}.tabulator-edit-select-list .tabulator-edit-select-list-item.active{color:#fff;background:#1d68cd}.tabulator-edit-select-list .tabulator-edit-select-list-item.active.focused{outline:1px solid hsla(0,0%,100%,.5)}.tabulator-edit-select-list .tabulator-edit-select-list-item.focused{outline:1px solid #1d68cd}.tabulator-edit-select-list .tabulator-edit-select-list-item:hover{cursor:pointer;color:#fff;background:#1d68cd}.tabulator-edit-select-list .tabulator-edit-select-list-notice{padding:4px;text-align:center}.tabulator-edit-select-list .tabulator-edit-select-list-group{border-bottom:1px solid #ddd;padding:4px;padding-top:6px;color:#333;font-weight:700}.tabulator.tabulator-ltr{direction:ltr}.tabulator.tabulator-rtl{text-align:initial;direction:rtl}.tabulator.tabulator-rtl .tabulator-header .tabulator-col{text-align:initial;border-left:1px solid #ddd;border-right:initial}.tabulator.tabulator-rtl .tabulator-header .tabulator-col.tabulator-col-group .tabulator-col-group-cols{margin-right:0;margin-left:-1px}.tabulator.tabulator-rtl .tabulator-header .tabulator-col.tabulator-sortable .tabulator-col-title{padding-right:0;padding-left:25px}.tabulator.tabulator-rtl .tabulator-header .tabulator-col .tabulator-col-content .tabulator-arrow{left:8px;right:auto}.tabulator.tabulator-rtl .tabulator-row .tabulator-cell{border-right:initial;border-left:1px solid #ddd}.tabulator.tabulator-rtl .tabulator-row .tabulator-cell .tabulator-data-tree-branch{margin-right:0;margin-left:5px;border-bottom-left-radius:0;border-bottom-right-radius:1px;border-left:initial;border-right:2px solid #ddd}.tabulator.tabulator-rtl .tabulator-row .tabulator-cell .tabulator-data-tree-control{margin-right:0;margin-left:5px}.tabulator.tabulator-rtl .tabulator-col-resize-handle{position:absolute;left:0;right:auto}.tabulator.tabulator-rtl .tabulator-col-resize-handle.prev{right:0;left:auto}.tabulator-print-fullscreen{position:absolute;top:0;bottom:0;left:0;right:0;z-index:10000}body.tabulator-print-fullscreen-hide>:not(.tabulator-print-fullscreen){display:none!important}.tabulator-print-table{border-collapse:collapse}.tabulator-print-table .tabulator-print-table-group{box-sizing:border-box;border-bottom:1px solid #999;border-right:1px solid #ddd;border-top:1px solid #999;padding:5px;padding-left:10px;background:#fafafa;font-weight:700;min-width:100%}.tabulator-print-table .tabulator-print-table-group:hover{cursor:pointer;background-color:rgba(0,0,0,.1)}.tabulator-print-table .tabulator-print-table-group.tabulator-group-visible .tabulator-arrow{margin-right:10px;border-left:6px solid transparent;border-right:6px solid transparent;border-top:6px solid #666;border-bottom:0}.tabulator-print-table .tabulator-print-table-group.tabulator-group-level-1 td{padding-left:30px!important}.tabulator-print-table .tabulator-print-table-group.tabulator-group-level-2 td{padding-left:50px!important}.tabulator-print-table .tabulator-print-table-group.tabulator-group-level-3 td{padding-left:70px!important}.tabulator-print-table .tabulator-print-table-group.tabulator-group-level-4 td{padding-left:90px!important}.tabulator-print-table .tabulator-print-table-group.tabulator-group-level-5 td{padding-left:110px!important}.tabulator-print-table .tabulator-print-table-group .tabulator-arrow{display:inline-block;width:0;height:0;margin-right:16px;border-top:6px solid transparent;border-bottom:6px solid transparent;border-right:0;border-left:6px solid #666;vertical-align:middle}.tabulator-print-table .tabulator-print-table-group span{margin-left:10px;color:#666}.tabulator-print-table .tabulator-data-tree-branch{display:inline-block;vertical-align:middle;height:9px;width:7px;margin-top:-9px;margin-right:5px;border-bottom-left-radius:1px;border-left:2px solid #ddd;border-bottom:2px solid #ddd}.tabulator-print-table .tabulator-data-tree-control{display:-ms-inline-flexbox;display:inline-flex;-ms-flex-pack:center;justify-content:center;-ms-flex-align:center;align-items:center;vertical-align:middle;height:11px;width:11px;margin-right:5px;border:1px solid #333;border-radius:2px;background:rgba(0,0,0,.1);overflow:hidden}.tabulator-print-table .tabulator-data-tree-control:hover{cursor:pointer;background:rgba(0,0,0,.2)}.tabulator-print-table .tabulator-data-tree-control .tabulator-data-tree-control-collapse{display:inline-block;position:relative;height:7px;width:1px;background:transparent}.tabulator-print-table .tabulator-data-tree-control .tabulator-data-tree-control-collapse:after{position:absolute;content:\"\";left:-3px;top:3px;height:1px;width:7px;background:#333}.tabulator-print-table .tabulator-data-tree-control .tabulator-data-tree-control-expand{display:inline-block;position:relative;height:7px;width:1px;background:#333}.tabulator-print-table .tabulator-data-tree-control .tabulator-data-tree-control-expand:after{position:absolute;content:\"\";left:-3px;top:3px;height:1px;width:7px;background:#333}\n";
styleInject(css_248z$1);

var ReactTabulator = require('react-tabulator')['ReactTabulator'];

function merge2Dicts(a, b) {
  // not inplace
  // overrides with 2nd dict if a key is same in both dicts
  var out = {};

  for (var key in a) {
    out[key] = a[key];
  }

  for (var _key in b) {
    out[_key] = b[_key];
  }

  return out;
}

function AcogTabulator(props) {
  // const md_match = useMediaQuery(theme.breakpoints.down("md"));
  var ref = React.useRef(null); // const {advancedToggle} = useContext(advancedModeContext)

  var _ref = props.advancedModeContext ? props.advancedModeContext : false,
      advancedToggle = _ref.advancedToggle;

  var multivalcol = props.multivalcol ? props.multivalcol : [];
  var htmlcol = props.htmlcol ? props.htmlcol : [];
  var iconcol = props.iconcol ? props.iconcol : [];
  var noSortCol = props.noSortCol ? props.noSortCol : []; // console.log(props.showcols)

  var columns = [];

  var _useState = React.useState(columns),
      _useState2 = _slicedToArray(_useState, 2),
      finalcolumns = _useState2[0],
      SetFinalColumns = _useState2[1];

  React.useEffect(function () {
    SetFinalColumns(columns);
  }, [props.showcols]);
  React.useEffect(function () {
    if (ref && ref.current && ref.current.table) {
      ref.current.table.redraw(true); //  console.log('redraw')
    }
  }); //  console.log(finalcolumns)
  //  console.log(props.data)

  var updateColProps = function updateColProps(tmp, props, col) {
    if (props.columnOptions && props.columnOptions[col]) {
      tmp["title"] = props.columnOptions[col]["title"] ? props.columnOptions[col]["title"] : tmp["title"];
      tmp["headerFilter"] = props.columnOptions[col]["search"] === false ? "" : tmp["headerFilter"];
    }
  };

  if (props.data === undefined || props.data === null || props.data.length === 0) {
    return null;
  }

  var column_names = Object.keys(props.data[0]).filter(function (name) {
    return name.startsWith("__") !== true && name !== "tableData" && htmlcol.includes(name) !== true && multivalcol.includes(name) !== true && iconcol.includes(name) !== true;
  });

  var _iterator = _createForOfIteratorHelper(column_names),
      _step;

  try {
    for (_iterator.s(); !(_step = _iterator.n()).done;) {
      var col = _step.value;
      var tmp = {
        title: col.toUpperCase(),
        field: col,
        formatter: "textarea",
        visible: props.showcols !== undefined ? props.showcols[col] : true,
        headerFilter: "input",
        headerSort: noSortCol.length > 0 && noSortCol.includes(col) ? false : true
      };
      updateColProps(tmp, props, col);
      columns.push(tmp);
    }
  } catch (err) {
    _iterator.e(err);
  } finally {
    _iterator.f();
  }

  var _iterator2 = _createForOfIteratorHelper(htmlcol),
      _step2;

  try {
    for (_iterator2.s(); !(_step2 = _iterator2.n()).done;) {
      var hcol = _step2.value; //  console.log(columns)

      if (Object.keys(props.data[0]).includes(hcol)) {
        var _tmp = {
          title: hcol.toUpperCase(),
          field: hcol,
          formatter: function formatter(cell, formatterParams) {
            var value = cell.getValue();
            var field = "<p style='white-space: normal; word-wrap: break-word;'>" + value + "</p>";
            return field;
          },
          visible: 'showcols' in props ? props.showcols[hcol] : true,
          headerFilter: "input",
          headerSort: noSortCol.length > 0 && noSortCol.includes(hcol) ? false : true
        };
        updateColProps(_tmp, props, col);
        columns.push(_tmp);
      }
    }
  } catch (err) {
    _iterator2.e(err);
  } finally {
    _iterator2.f();
  }

  var _iterator3 = _createForOfIteratorHelper(multivalcol),
      _step3;

  try {
    for (_iterator3.s(); !(_step3 = _iterator3.n()).done;) {
      var mcol = _step3.value;

      if (Object.keys(props.data[0]).includes(mcol)) {
        var _tmp2 = {
          title: mcol.toUpperCase(),
          field: mcol,
          formatter: function formatter(cell, formatterParams) {
            var value = cell.getValue();
            var field = "<ul>";

            if (typeof value === "string") {
              // console.log(value)
              value = JSON.parse(value);
            } // console.log(value)


            var _iterator5 = _createForOfIteratorHelper(value),
                _step5;

            try {
              for (_iterator5.s(); !(_step5 = _iterator5.n()).done;) {
                var val = _step5.value;
                field += "<li style='white-space: normal; word-wrap: break-word;'>" + val + "</li>";
              }
            } catch (err) {
              _iterator5.e(err);
            } finally {
              _iterator5.f();
            }

            field += "</ul>";
            return field;
          },
          visible: 'showcols' in props ? props.showcols[mcol] : true,
          headerFilter: "input",
          headerSort: noSortCol.length > 0 && noSortCol.includes(mcol) ? false : true
        };
        updateColProps(_tmp2, props, col);
        columns.push(_tmp2);
      }
    }
  } catch (err) {
    _iterator3.e(err);
  } finally {
    _iterator3.f();
  }

  var _iterator4 = _createForOfIteratorHelper(iconcol),
      _step4;

  try {
    for (_iterator4.s(); !(_step4 = _iterator4.n()).done;) {
      var icol = _step4.value;

      if (Object.keys(props.data[0]).includes(icol)) {
        var _tmp3 = {
          title: icol.toUpperCase(),
          field: icol,
          formatter: function formatter(cell, formatterParams) {
            var value = cell.getValue();
            return "<span style='color:" + value['color'] + "; font-weight:bold;'>" + value['val'] + "</span>";
          },
          tooltip: function tooltip(cell) {
            var type = cell.getValue()["type"];
            return type;
          },
          sorter: function sorter(a, b, aRow, bRow, column, dir, sorterParams) {
            // console.log(a)
            // console.log(b)
            return a['val'] - b['val'];
          },
          visible: props.showcols[icol]
        };
        updateColProps(_tmp3, props, col);
        columns.push(_tmp3);
      }
    }
  } catch (err) {
    _iterator4.e(err);
  } finally {
    _iterator4.f();
  }

  var options = {
    movableColumns: true,
    downloadReady: function downloadReady(fileContents, blob) {
      return blob;
    }
  };

  var downloadExcel = function downloadExcel() {
    // let xlsx = require("xlsx")
    var fileName = props.title;

    if (!('title' in props)) {
      fileName = "export";
    }

    ref.current.table.download('csv', "".concat(fileName, ".csv"));
  };

  if (finalcolumns.length === 0) {
    return null;
  }

  if ('options' in props) {
    options = merge2Dicts(options, props.options);
  }

  if ('pagination' in props && props.pagination === false) {
    options['pagination'] = false;
  } else {
    options['pagination'] = "local";
    options['paginationSize'] = 10;
  }

  if (finalcolumns && finalcolumns.length !== 0) {
    columns = add_sorter_param_to_doses(finalcolumns);
  }

  return /*#__PURE__*/React__default['default'].createElement(React.Fragment, null, advancedToggle && [/*#__PURE__*/React__default['default'].createElement(Button, {
    variant: "contained",
    onClick: downloadExcel
  }, "Export")], /*#__PURE__*/React__default['default'].createElement(ReactTabulator, _extends({
    ref: ref,
    layout: "fitColumns",
    columns: finalcolumns,
    data: props.data,
    options: options,
    rowClick: props.rowClick
  }, props.additional_props)));
}

function add_sorter_param_to_doses(columns) {
  var new_cols = [];

  var _iterator6 = _createForOfIteratorHelper(columns),
      _step6;

  try {
    for (_iterator6.s(); !(_step6 = _iterator6.n()).done;) {
      var col = _step6.value;

      if (col && col.title && col.title === "DOSES") {
        col["sorter"] = "number";
      }

      new_cols.push(col);
    }
  } catch (err) {
    _iterator6.e(err);
  } finally {
    _iterator6.f();
  }

  return new_cols;
}

exports.AcogTabulator = AcogTabulator;
